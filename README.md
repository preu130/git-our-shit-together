:warning: the slides in this repo still use `master` instead of the more modern `main` terminology. I am working on adapting this. :warning: 

# Talk Slides
- [https://frie.codes/page/gost/](https://frie.codes/page/gost/): under *Slides*, find your talk.

# Playground repository
- [https://gitlab.com/friep/git-our-shit-together-test-repo](https://gitlab.com/friep/git-our-shit-together-test-repo)

# Requirements for Hands-On Sessions

## create a GitLab account
- [https://gitlab.com/users/sign_in](https://gitlab.com/users/sign_in)

## install VSCode editor
- [https://code.visualstudio.com/download](https://code.visualstudio.com/download)

## install git
- Windows: [https://gitforwindows.org/](https://gitforwindows.org/)
- Mac: [https://git-scm.com/downloads](https://git-scm.com/downloads)
- Linux: use your package manager or [https://git-scm.com/downloads](https://git-scm.com/downloads)

## install vscode extensions
-> see slides

## create an ssh key
-> see slides
